<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="" ="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Week6 Exercise</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" type="Image/png" href="Images/logo.png">
</head>
<body id="contactus-pg">
<header>

<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" id="nav-bar">
 <div class="container">
  <a class="navbar-brand" href="index.html">
      <img src="Images/logo.png" alt="logo">
  </a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="#navbarTogglerDem02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0" id="nav-center">
      <li class="nav-item">
        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/gallery.php">Gallery</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/movies.php">Movies</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/contactus.php">Contact Us</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Form</a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
  </div>
  </div>
</nav>
  </header>

<div class="bg-site">
      <div class="container">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
  </ol>
</nav>

<section class="form-section">
          <p class="about-company">BBC is the best cinema in the world. We have the latest movies that keeps everyone entertained. Our movies range from Nollywood, bollywood to Hollywood.We also have animations and cartoons for kids. For daily feeds on our recent movies, you can subscribe below. </p>

<!--IF/ELSE Statement-->
<?php if ( isset($_POST['name']) && isset($_POST['email']) && isset($_POST['message']) ): ?>

  <?php

  //IMPORT CLASS HERE
  include "classes/ContactUs.php";

  //Execute with the custom static function
  ContactUs::process($_POST);
  ?>

<div class="container" align="center">
  <h1>Thank You For Contacting Us!</h1>
  <p> An email has been forwarded to you.</p>
  <br>
  <a href="form.php">Contact Us</a>
</div>

<?php else: ?>

<div class="jumbotron">
<div class="container">
<h1>Form</h1>
<form action="form.php" method="POST">
  <label for="name">NAME:</label><br>
  <input type="text" id="name" name="name" placeholder="Full Name" required> <br>

  <label for="email">EMAIL:</label><br>
  <input type="email" id="email" name="email" placeholder="Enter your email"><br>

  <label for="message">MESSAGE:</label><br>
  <textarea id="message" name="message" rows="5" placeholder="Your message"></textarea><br>  
<hr>
  <input type="submit" value="submit">
    
</form>
</div>
</div>
<?php endif; ?>

<div class="end-page">BBC©</div>
</section>
</div>

<!-- FOOTER -->

<footer class="footer" id="footer">
<div class="container_12">
<div class="grid_12">
<div class="sub-copy">Copyright &copy; <span id="copyright-year"></span> | 2019 All Right Reserved</div>

<div class="row blue">
<div class="col-12">
<h3>Contact Us</h3>
<p><strong>Address:</strong> 254 Ahmadu Bello Way, Victoria Island, Lagos<br>
<strong>Phone No:</strong> 08183496881<br>
<strong>Office Hours:</strong> Weekdays 10am-5.30pm
</p>
</div>

</footer>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>