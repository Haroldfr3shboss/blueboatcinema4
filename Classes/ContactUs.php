<?php

require 'vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class ContactUs
{
    public static function process($param)
    {
        date_default_timezone_set("Africa/Lagos");

        //open/Create contact.csv
        $file = fopen('contact.csv', 'a');

        //Create csv columns (Run this Once)
        fputcsv($file, array('Name', 'Email', 'Message', 'Date'));

        //Append Contact Details to CSV file
        fputcsv($file, array($param['name'], $param['email'], $param['message'], date("Y-m-d h:i:sa")));

        //Close File Stream
        fclose($file);

        //Prepare Email Message and Send to user
        $message = "Hello " . $param['name'] . ",\n\nThank you for reaching out.\n\nRegards.";
        $headers = "Reply-To: Movie Admin <abrahamharold22@gmail.com>\r\n";
        $headers .= "Return-Path: Movie Admin <abrahamharold22@gmail.com\r\n";
        $headers .= "From: Movie Admin <abrahamharold22@gmail.com\r\n";

        //SEND EMAIL
        mail($param['email'], "Booust Contact", $message, $headers);

        /* Read Information From Csv File */

        $csv = array_map('str_getcsv', file('contact.csv'));
        echo $csv[sizeof($csv) - 1][0] . '<br>';
        echo $csv[sizeof($csv) - 1][1] . '<br>';
        echo $csv[sizeof($csv) - 1][2] . '<br>';
        echo $csv[sizeof($csv) - 1][3] . '<br>';


        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //Server settings
            $mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $mail->isSMTP();                                      // Set mailer to use SMTP
            $mail->Host = 'ssl://smtp.gmail.com';  // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                               // Enable SMTP authentication
            $mail->Username = 'abrahamharold22@gmail.com';                 // SMTP username if you're using gmail
            $mail->Password = '=freshboss123';                           // SMTP password
            $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
            $mail->Port = '465';                                    // TCP port to connect to.. 465 or 587

            //sender
            $mail->setFrom('abrahamharold22@gmail.com', 'Mr blue');

            //Recipient
            $mail->addAddress($param['email'], $param['email']);     // Add a recipient using the parameter you collected on your contact form

            //$mail->addReplyTo('anyemail@domain.com', 'Jay Cole');
            //$mail->addCC('youCanCopyAnybody@domain.com');
            //$mail->addBCC('blindCopy@domain.com');

            //Attachments are optional
            //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            //Content
            //$mail->isHTML(true);                                  // Set email format to HTML
            //$mail->Subject = 'Booust com';
            //$mail->Body = 'the body of the email';       //you can use a variable to define your body.

//difference between AltBody and body is that one is plain text and the other is not

            $mail->AltBody = strip_tags($message);

            $mail->send();

        } catch (Exception $e) {    //i personally on my exception i used: echo ''
            echo '';
        }
    }
}
?>