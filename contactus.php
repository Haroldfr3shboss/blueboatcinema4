<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="" ="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Week6 Exercise</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" type="Image/png" href="Images/logo.png">
</head>
<body id="contactus-pg">
<header>

<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" id="nav-bar">
 <div class="container">
  <a class="navbar-brand" href="index.html">
      <img src="Images/logo.png" alt="logo">
  </a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="#navbarTogglerDem02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0" id="nav-center">
      <li class="nav-item">
        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/gallery.php">Gallery</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/movies.php">Movies</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Contact Us</a>
      </li>
        <li class="nav-item">
            <a class="nav-link" href="/form.php">Form</a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
  </div>
  </div>
</nav>
  </header>

<div class="bg-site">
      <div class="container">
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="#">Home</a></li>
    <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
  </ol>
</nav>

<section class="form-section">
          <p class="about-company">BBC is the best cinema in the world. We have the latest movies that keeps everyone entertained. Our movies range from Nollywood, bollywood to Hollywood.We also have animations and cartoons for kids. For daily feeds on our recent movies, you can subscribe below. </p>

<form class="contact-us">
<h1>Contact Form</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    <small id="email_error" class="form-text flag-error"></small>
  </div>
  <div class="form-group">
    <label for="exampleInputFullname1">Full Name</label>
    <input type="name" class="form-control" id="exampleInputFullname1" placeholder="Enter Full Name">
    <small id="fullname_error" class="form-text flag-error"></small>
  </div>
  <div class="form-group">
    <label for="exampleInputPhonenumber1">Phone</label>
    <input type="tel" class="form-control" id="exampleInputPhonenumber1" placeholder="Phone Number">
    <small id="phonenumber_error" class="form-text flag-error"></small>
  </div>
  <div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Save my details</label>
  </div>
  <span id="submitForm" class="btn btn-primary">Submit</span>

  <hr>

  <span id="hideBtn" class="btn btn-sm btn-default">Hide</span> | <span id="showBtn" class="btn btn-sm btn-secondary">Show</span>
    
</form>
<div class="end-page">BBC©</div>
</section>
</div>
</div>

<!-- FOOTER -->

<footer class="footer" id="footer">
<div class="container_12">
<div class="grid_12">
<div class="sub-copy">Copyright &copy; <span id="copyright-year"></span> | 2019 All Right Reserved</div>

<div class="row blue">
<div class="col-12">
<h3>Contact Us</h3>
<p><strong>Address:</strong> 254 Ahmadu Bello Way, Victoria Island, Lagos<br>
<strong>Phone No:</strong> 08183496881<br>
<strong>Office Hours:</strong> Weekdays 10am-5.30pm
</p>
</div>

</footer>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

<script>
$("#hideBtn").click(function(event){// Hide Button
event.preventDefault();
$('#submitForm').hide();
});

$("#showBtn").click(function(event){// Hide Button
event.preventDefault();
$('#submitForm').show();
});

$('#submitForm').click(function(){// submit form
var emailAddress = $('#exampleInputEmail1').val();
var fullName = $('#exampleInputFullname1').val();
var phoneNumber = $('#exampleInputPhonenumber1').val();

if(emailAddress === ""){
  $('#email_error').text('Please enter your email address');
}else{
  $('#email_error').text('');
}

if(fullName === ""){
  $('#fullname_error').text('Please enter your Full name');
}else{
  $('#fullname_error').text('');
}

if(phoneNumber === ""){
  $('#phonenumber_error').text('Phone number is needed');
}else{
  $('#phonenumber_error').text('');
}


});

</script>
</body>
</html>