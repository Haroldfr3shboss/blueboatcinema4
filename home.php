<?php

//Begin Session
session_start();

?>

<!DOCTYPE html>
<html>
<head>
    <title>Home Page</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style2.css">

</head>
<body>


<?php if ( isset($_SESSION['name']) ): ?>

    <div class="container" align="center">
        <div align="right"><a href="login.php">Log out</a></div>
        <h1>Welcome, <?php echo $_SESSION['name']; ?>!</h1>
    </div>

<?php else: ?>
    <div class="container" align="center">
        <div align="right"><a href="login.php">Login</a></div>
        <h1>My Movies</h1>
    </div>


<?php endif; ?>

</body>
</html>
