<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Week6 Exercise</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" type="Image/png" href="Images/logo.png">
</head>
<body id="movies-pg">
<header>
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
 <div class="container">
  <a class="navbar-brand" href="index.html">
      <img src="Images/logo.png" alt="logo">
  </a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="#navbarTogglerDem02">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0" id="nav-center">
      <li class="nav-item">
        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/gallery.php">Gallery</a>
      </li>
      <li class="nav-item active">
        <a class="nav-link" href="#">Movies</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/contactus.php">Contact Us</a>
      </li>
        <li class="nav-item">
            <a class="nav-link" href="form.php">Form</a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
  </form>
  </div>
</nav>
</header>

<div class="bg-site">
      <div class="container">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item go-home"><a href="index.html">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Movies</li>
          </ol>
        </nav>
        
        <article class="film-section">Best Watched</article>
        <hr>
        <div class="row movie-gallery">
          <div class="col-sm">

            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#hotdogModal">
                <div class="img-box">
                  <img src="Images/hotdog.jpg" class="modal-img">
                  <div class="overlay">
                      <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="hotdogModal" tabindex="-1" role="dialog" aria-labelledby="hotdogModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="hotdogModalLabel">HotDog</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/hotdog.jpg">
                    <span>Release date - 2018</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#jumanjiModal">
                <div class="img-box">
                  <img src="Images/jumanji.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="jumanjiModal" tabindex="-1" role="dialog" aria-labelledby="jumsnjiModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="jumanjiModalLabel">Jumanji</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/jumanji.jpg">
                    <span>Release date - 2018</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#pantherModal">
                <div class="img-box">
                  <img src="Images/panther.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="pantherModal" tabindex="-1" role="dialog" aria-labelledby="pantherModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="pantherModalLabel">Black Panther1</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/panther.jpg">
                    <span>Release date - 2018</span>                
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#jurasicModal">
                <div class="img-box">
                  <img src="Images/jurasic.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="jurasicModal" tabindex="-1" role="dialog" aria-labelledby="jurasicModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="jurasicModalLabel">Jurasic World</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/jurasic.jpg">
                    <span>Release date - 2018</span>                    
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#shankerModal">
                <div class="img-box">
                  <img src="Images/shanker.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500/span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="shankerModal" tabindex="-1" role="dialog" aria-labelledby="shankerModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="shankerModalLabel">Shanker</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/shanker.jpg">
                    <span>Release Year - 2018</span>                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <article class="film-section">Thrillers</article>
        <hr>
        <div class="row movie-gallery">
          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#panther2Modal">
                <div class="img-box">
                  <img src="Images/panther2.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="panther2Modal" tabindex="-1" role="dialog" aria-labelledby="panther2ModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="panther2ModallLabel">Black Panther 2</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/panther2.jpg">
                    <span>Release Year - 2018</span>                    
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#strangersModal">
                <div class="img-box">
                  <img src="Images/strangers.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="strangersModal" tabindex="-1" role="dialog" aria-labelledby="strangersModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="strangersModalLabel">Stranger</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/strangers.jpg">
                    <span>Release Year - 2018</span>                  
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#wolverineModal">
                <div class="img-box">
                  <img src="Images/wolverine.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="wolverineModal" tabindex="-1" role="dialog" aria-labelledby="wolverineModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="wolverineModalLabel">Wolverine</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <a href=""><img src="Images/wolverine.jpg"></a>
                    <span>Release Year - 2018</span>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- anchor trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#mazeModal">
                <div class="img-box">
                  <img src="Images/maze.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="mazeModal" tabindex="-1" role="dialog" aria-labelledby="mazeModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="mazeModalLabel">Maze Runner</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/maze.jpg">
                    <span>Release Year - 2018</span>                    
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="col-sm">
            <!-- image trigger modal -->
            <div class="movie-card">
              <a data-toggle="modal" data-target="#securityModal">
                <div class="img-box">
                  <img src="Images/security.jpg" class="modal-img">
                  <div class="overlay">
                    <span>More info</span>
                  </div>
                </div>
              </a>
              <button type="button" class="btn rent">Rent</button><span> N500</span>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="securityModal" tabindex="-1" role="dialog" aria-labelledby="securityModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="securityModalLabel">Security</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <img src="Images/security.jpg">
                    <span>Release Year - 2018</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<!-- FOOTER -->

<footer class="footer" id="footer">
<div class="container_12">
<div class="grid_12">
<div class="sub-copy">Copyright &copy; <span id="copyright-year"></span> | 2019 All Right Reserved</div>

<div class="row blue">
<div class="col-12">
<h3>Contact Us</h3>
<p><strong>Address:</strong> 254 Ahmadu Bello Way, Victoria Island, Lagos<br>
<strong>Phone No:</strong> 08183496881<br>
<strong>Office Hours:</strong> Weekdays 10am-5.30pm
</p>
</div>

</footer>

<script src="js/jquery.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>
</html>