<?php

//Begin Session
session_start();

//Lets include database connection
include 'config/database.php';

if( isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) ){
    try{

        //Insert query
        $query = "INSERT INTO users SET name=:name, email=:email, password=:password, created_at=:created_at";

        //prepare query for execution
        $stmt = $connection->prepare($query);

        //Posted values
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];

        //bind the parameters
        $stmt->bindParam(':name',$name);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', hash ('sha256', $password));
        $stmt->bindParam(':created_at', date ('Y-m-d H:i:s'));

        //Execute the query
       if ($stmt->execute()){

            $_SESSION['name'] = $name;
            header("Location: home.php");
            exit();
        }else{
        echo "<div class='alert alert-danger'>Unable to save record.</div>";
       }
    }
        //to show error
    catch(PD0Exception $exception){
        die('ERROR:' . $exception->getMessage());
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Register Page</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style2.css">

 </head>

<body>

<div class="container" align="center">
    <h1>Register</h1>
</div>

<div class="container">

    <form action="register.php" method="POST">

        <label for="text">FIRST NAME</label>
        <input type="text" id="text" name="name" required><br>

        <label for="email">EMAIL</label>
        <input type="email" id="email" name="email" required><br>

        <label for="password">PASSWORD</label>
        <input type="password" id="password" name="password" required><br>

        <input type="submit" value="submit">
    </form>
    <br>
    <p>I have an account. <a href="login.php">Login</a></p>
</div>

</body>
</html>
