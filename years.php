<?php

//Initialize Start Year
$timestamp = strtotime(  '1980');
$year = date(  'Y', $timestamp);

//Initialize Stop Year
$timestamp = strtotime( '2018');
$stopYear = date( 'Y', $timestamp);

//Initialize Leap Year Counter
$counter = 0;

//Run a while Loop to Print Year
while ($year <= $stopYear){

    if(isLeapYear($year)){
        $counter = $counter + 1;
        echo $year . ' Leap Year ' . '<br>';
    }
    else
        echo $year . '<br>';

//Increment Year
$year = $year + 1;
}

//Echo Total number of leap years
echo '<br>' . 'Total number of Leap years = ' . $counter;

/* Ways to find a leap Year*/

/*
1. year % 4 == 0 -> Evenly Divisible by 4
2. AND year % 100 !=0 -> should not be evenly divisible by 100
3. OR year % 400 == 0 -> Evenly Divisible by 400

(if 1st AND 2nd are both true) or (3rd is true) then it is a leap year

*/


/* Function to determine a Leap year*/
function isLeapYear($year){

    $isLeapYear = false;
    if((($year % 4)==0 && ($year % 100)!=0)||($year % 400)==0)
        $isLeapYear = true;
    return $isLeapYear;

}