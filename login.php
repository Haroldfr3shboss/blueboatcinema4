<?php

//Begin Session
session_start();

//Lets include database connection
include 'config/database.php';

if( isset($_POST['email']) && isset($_POST['password']) ){
    try{

        //To select all data
        $query = "SELECT name FROM users WHERE email=:email AND password=:password";
        $stmt = $connection->prepare($query);

        //To bind the parameters
        $email = $_POST['email'];
        $password = hash('sha256', $_POST['password']);

        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':password', $password);
        $stmt->execute();

        //To get number of rows returned
        $num = $stmt->rowCount();

        if($num>0){
            $row = $stmt->fetch();
            $_SESSION['name'] = $row['name'];
            header("Location: home.php");
            exit();
        }
    }
    //to show error
    catch(PDOException $exception){
        die('ERROR:' . $exception->getMessage());
    }
}

else{

    session_destroy();
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style2.css">

 </head>

 <body>

<div class="container" align="center">
    <h1>Log In</h1>
</div>

<div class="container">

    <form action="login.php" method="POST">

        <label for="email">EMAIL</label>
        <input type="email" id="email" name="email" required>

        <label for="password">PASSWORD</label>
        <input type="password" id="password" name="password" required>

        <input type="submit" value="submit">
    </form>
    <br>
    <p>Don't have an account? <a href="register.php">Register</a></p>
</div>

</body>
</html>
